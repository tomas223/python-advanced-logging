import logging
from logging.config import dictConfig
from fastapi import FastAPI

from app.core.log_config import app_dict_config, init_loggers

# This should run as soon as possible to catch all logs
# Run only one of these
# init_loggers()
dictConfig(app_dict_config)

# FastApi app
app = FastAPI()

# init our logger
log = logging.getLogger("simple_example")

# first endpoint
@app.get("/log_now")
def log_now():
    log.debug("/api/log_now starts")
    log.info("I'm logging ...")
    log.warning("some warnings")
    log.error("Uff!")
    log.critical("This was inevitable!")

    return {"result": "OK"}
