import logging
import uvicorn

LOGGING_LEVEL: str = logging.DEBUG
# FORMAT: str = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
FORMAT: str = "%(levelprefix)s %(asctime)s | %(message)s"

app_dict_config = {
    "version": 1,  # mandatory field
    # this should be false, otherwise
    # default uvicorn logs will be supressed
    "disable_existing_loggers": False,
    "formatters": {
        "basic": {
            "()": "uvicorn.logging.DefaultFormatter",
            "format": FORMAT,
            "datefmt": "%Y-%m-%d %H:%M:%S",
        }
    },
    "handlers": {
        "console": {
            "formatter": "basic",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",
            "level": LOGGING_LEVEL,
        }
    },
    "loggers": {
        "simple_example": {
            "handlers": ["console"],
            "level": LOGGING_LEVEL,
            # "propagate": False
        }
    },
}


def init_loggers(logger_name: str = "simple_example"):
    # create logger
    logger = logging.getLogger("simple_example")
    logger.setLevel(LOGGING_LEVEL)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(LOGGING_LEVEL)

    # create formatter
    # formatter = logging.Formatter(FORMAT)
    formatter = uvicorn.logging.DefaultFormatter(FORMAT, datefmt="%Y-%m-%d %H:%M:%S")

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
